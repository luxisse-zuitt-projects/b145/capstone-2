// Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/userRoutes');
	const productRoutes = require('./routes/productRoutes');
	const orderRoutes = require('./routes/orderRoutes');



// Environment Variables Setup
	dotenv.config();
	const secret = process.env.MONGO_URL;



// Server Setup
	const app = express();
	const port = process.env.PORT || 4000;
	app.use(express.json());
	app.use(express.urlencoded({extended : true}));
	app.use(cors());



// Database Connect
	mongoose.connect(secret,
		{
			useNewUrlParser : true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection;

		db.on("error", console.error.bind(console, "There is an error with the connection"))
		db.once("open", () => console.log("Successfully connected to the database"))



// Server Routes
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);



// Server Response
	app.listen(port, () =>
	console.log(`Server is running at port ${port}`)
	);