const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth');



// NON-ADMIN USER CHECKOUT (Create Order) 
	router.post("/addOrder", auth.verify, (req, res) => {
		
		const data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			reqBody: req.body,
		};
		
		orderController
			.addOrder(data)
			.then(resultFromController => 
				res.send(resultFromController));
	});



// RETRIEVE AUTHENTICATED USER'S ORDERS
	router.get('/listOrders', auth.verify, (req, res) => {
		
		const userId = auth.decode(req.headers.authorization).id;
			
		orderController
			.listOrders(userId)
			.then(resultFromController => 
				res.send(resultFromController))
	});



// RETRIEVE ALL ORDERS (Admin Only)
	router.get('/allOrders', auth.verify, (req, res) => {
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;

		orderController
			.allOrders(isAdmin)
			.then(resultFromController =>
				res.send(resultFromController))
	});



module.exports = router;