const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// NON-ADMIN USER CHECKOUT (Create Order) 
	module.exports.addOrder = async (data) => {
		
		// If non-admin creates an order
		if (data.isAdmin !== true) {
			
			let newOrder = await new Order({
				userId: data.reqBody.userId,
				products: data.reqBody.products,
				totalAmount: data.reqBody.totalAmount
			});
			
			// Saves the created object to our database
			return newOrder
				.save()
				.then((order, err) => {
					
					// Product creation failed	
					if (err) {
						return err;
					}else{
						return `Product added ${order}`;
						// return order;
					}
			});
		
		// User is an admin
		}else{
			return `You don't have the correct permissions to place an order`;
		}
	};



// RETRIEVE AUTHENTICATED USER'S ORDERS
	module.exports.listOrders = (userId) => {
	
		return Order
			.find({userId : userId})
			.then(result => {

				// No existing orders
				if(result.length === 0){
					return `No existing orders`;
				
				// Listing current orders
				}else{
					return `Your current orders ${result}`;
				};
		});
	};



// RETRIEVE ALL ORDERS (Admin Only)
	module.exports.allOrders = (isAdmin) => {

		if(isAdmin === true) {
			return Order
				.find({})
				.then(result => {
					return `Here's a list of all orders ${result}`;
				});
		
		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};