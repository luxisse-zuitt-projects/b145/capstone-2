const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// USER REGISTRATION
	module.exports.registerUser = async (reqBody) => {

		return User
		.find({email : reqBody.email})
		.then(result => {
			
			if(result.length > 0) {
				return `This account already exists!`;
			
			}else{
				
				let newUser = new User({
					email : reqBody.email,
					password : bcrypt.hashSync(reqBody.password, 10)
				});

				return newUser
				.save()
				.then((user, err) => {

					if(err){
						return false;
					
					}else{
						return `Account successfully registered ${user}`;
					};
				});
			};
		});	
	};



// USER LOGIN
	module.exports.loginUser = async (reqBody) => {

		return User
		.findOne({email : reqBody.email})
		.then(result => {

			// No user account found
			if(result == null){
				return `Please enter a valid email address.`;
			
			}else{
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				
				// Allow log in if password matches
				if(isPasswordCorrect){
					return {access : auth.createAccessToken(result)};
				
				// Incorrect password
				}else {
					return `Sorry, that password isn't right.`;
				};
			};
		});
	};



// SET USER AS ADMIN (ADMIN ONLY)
	module.exports.assignAdmin = async (data) => {
	console.log(data)
		
		if(data.isAdmin === true) {

			let isNewAdmin = await User
			.findById(data.userId)
			.then((result, err) => {
			 	result.isAdmin = true;

			 	return result
			 	.save()
			 	.then((newAdmin, err) => {

					if (err) {
						return false;
						
					}else{
						return true;
					};
				});
			});

		if(isNewAdmin){
			return `You have assigned a new admin`;
		};

		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};



// RETRIEVE ALL USERS (Admin Only)
	module.exports.allUsers = (isAdmin) => {

		if(isAdmin === true) {
			return User
				.find({})
				.then(result => {
					return `Here's a list of all users ${result}`;
				});
		
		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};



// RETRIEVE ALL ADMINS (Admin Only)
	module.exports.allAdmins = (isAdmin) => {

		if(isAdmin === true) {
			return User
				.find({isAdmin: true})
				.then(result => {
					return `Here's a list of all current admins ${result}`;
				});
		
		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};