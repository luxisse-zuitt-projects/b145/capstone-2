const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// CREATE PRODUCT (Admin Only)
	module.exports.addProduct = async (data) => {

		// User is an admin
		if (data.isAdmin === true) {

			const product = await Product
			.find({name : data.reqBody.name});

			// Product already exists
			if(product.length > 0) {
				return `This product already exists!`;
			}

			let newProduct = await new Product({
				name : data.reqBody.name,
				description : data.reqBody.description,
				price : data.reqBody.price
			});

			// Saves the created object to our database
			return newProduct
			.save()
			.then((product, err) => {

				// Product creation failed
				if (err) {
					return false;

				// Product creation successful
				}else{
					return `Product added ${product}`;
				};
			});

		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};



// RETRIEVE ALL ACTIVE PRODUCTS
	module.exports.activeProducts = () => {
	
		return Product
			.find({isActive : true})
			.then(result => {
				return result;
		});
	};



// RETRIEVE SINGLE PRODUCT
	module.exports.getProduct = (reqParams) => {

		return Product
			.findById(reqParams.productId)
			.then(result => {
				return result;
		});
	};



// UPDATE PRODUCT INFO (Admin Only)
	module.exports.updateProduct = (data) => {
	console.log(data);
	
		return Product
			.findById(data.productId)
			.then((result,err) => {
				console.log(result);
			
			if(data.isAdmin === true){

				result.name = data.updatedProduct.name
				result.description = data.updatedProduct.description
				result.price = data.updatedProduct.price
					
				console.log(result);

					return result
						.save()
						.then((updatedProduct, err) => {
			
							if(err){
								return false;
							
							}else{
								return `Product info has been updated ${updatedProduct}`;
							};
						});
			
			// User is not an admin
			}else{
				return `You must be an Admin to perform this action`;
			};
		});
	};



// ARCHIVE PRODUCT (Admin Only)
	module.exports.archiveProduct = async (data) => {

		if(data.isAdmin === true) {

			return Product
				.findById(data.productId)
				.then((result, err) => {

				result.isActive = false;

				return result
					.save()
					.then((archivedProduct, err) => {

						if(err) {
							return false;

						}else{
							return `This product has been archived ${result}`;
						};
				});
			});

		// User is not an admin
		}else{
			// return false;
			return `You must be an Admin to perform this action`;
		};
	};