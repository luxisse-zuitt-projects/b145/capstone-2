const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');



// USER REGISTRATION
	router.post('/register', (req, res) => {
		userController
			.registerUser(req.body)
			.then(resultFromController => 
				res.send(resultFromController))
	});



// USER LOGIN
	router.post('/login', (req, res) => {
		userController
			.loginUser(req.body)
			.then(resultFromController => 
				res.send(resultFromController))
	});



// SET USER AS ADMIN (ADMIN ONLY)
	router.put("/:userId", auth.verify, (req, res) => {
		const data = {
			userId: req.params.userId,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		};

		userController
			.assignAdmin(data)
			.then((resultFromController) =>
				res.send(resultFromController))
	});



// RETRIEVE ALL USERS (Admin Only)
	router.get('/allUsers', auth.verify, (req, res) => {
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;

		userController
			.allUsers(isAdmin)
			.then(resultFromController =>
				res.send(resultFromController))
	});



// RETRIEVE ALL ADMINS (Admin Only)
	router.get('/allAdmins', auth.verify, (req, res) => {
		
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;

		userController
			.allAdmins(isAdmin)
			.then(resultFromController =>
				res.send(resultFromController))
	});



module.exports = router;