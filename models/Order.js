const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId : {
		type : String,
		required : [true, "UserId of order owner is required"]
	},
	
	products: [
		{
			productId : {
				type : String,
				required : [true, "ProductId belonging to order is required"]
			},

			quantity : {
				type : Number,
				default : 0
			}
		}
	],

	totalAmount : {
		type: Number,
		required : [true, "Total amount ordered is required"]
	},

	purchasedOn : {
		type : Date,
		default : new Date()
	}
}, {timestamps : true});



module.exports = mongoose.model('Order', orderSchema);