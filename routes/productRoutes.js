const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');



// CREATE PRODUCT (Admin Only)
	router.post('/addProduct', auth.verify, (req, res) => {

		const data = {
			reqBody : req.body,
			isAdmin : auth.decode(req.headers.authorization).isAdmin
		};
		
		productController
			.addProduct(data)
			.then(resultFromController => 
				res.send(resultFromController))
	});



// RETRIEVE ALL ACTIVE PRODUCTS
	router.get('/activeProducts', (req, res) => {
	
	productController
		.activeProducts()
		.then(resultFromController => 
			res.send(resultFromController))
	});



// RETRIEVE SINGLE PRODUCT
	router.get('/:productId', (req, res) => {
	
	productController
		.getProduct(req.params)
		.then(resultFromController => 
			res.send(resultFromController))
	});



// UPDATE PRODUCT INFO (Admin Only)
	router.put('/:productId', auth.verify, (req, res) => {

		const data = {
			productId : req.params.productId,
			isAdmin : auth.decode(req.headers.authorization).isAdmin,
			// payload : auth.decode(req.headers.authorization),
			updatedProduct : req.body
		};

		productController
			.updateProduct(data)
			.then(resultFromController => 
				res.send(resultFromController))
});



// ARCHIVE PRODUCT (Admin Only)
	router.put('/:productId/archive', auth.verify, (req, res) => {

		const data = {
			productId : req.params.productId,
			// payload : auth.decode(req.headers.authorization)
			isAdmin : auth.decode(req.headers.authorization).isAdmin
		};

		productController
			.archiveProduct(data)
			.then(resultFromController => 
				res.send(resultFromController))
	});



module.exports = router;