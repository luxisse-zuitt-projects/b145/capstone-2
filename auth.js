const jwt = require('jsonwebtoken');
const secret = `${process.env.JWT_SECRET_KEY}`;



// Token Creation
	module.exports.createAccessToken = (user) => {

		const data = {
			id : user._id,
			email : user.email,
			isAdmin : user.isAdmin
		};

		return jwt.sign(data, secret, {})
	};

	

// Token Verification
	module.exports.verify = (req, res, next) => {
		
		let token = req.headers.authorization;

		if(typeof token !== "undefined") {

			console.log(token);
			
			token = token.slice(7, token.length);

			return jwt.verify(token, secret, (err, data) => {

				if(err){
					res.status(403).json("Invalid token!");
				}else{
					next();
				};
			});

		}else{
			return res.status(401).json("Authentication failed!");
		
		};
	};



// Token Decryption
	module.exports.decode = (token) => {

		if(typeof token !== "undefined") {

			token = token.slice(7, token.length);

			return jwt.verify(token, secret, (err, data) => {

				if(err){
					return null;
				}else{
					return jwt.decode(token, {complete: true}).payload;		
				};
			});

		}else{		
			return null;
		};
	};